package android2020.retrofit;

import java.util.ArrayList;

import android2020.model.Folder;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FolderService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("users/accounts/{id}/folders/view")
    Call<ArrayList<Folder>> getFolders(@Path("id") int id);

    @POST("/users/accounts/{id}/folders/{name}")
    Call<Folder> addFolder(
            @Path("id") int id,
            @Path("name") String name);

    @PUT("/users/accounts/folders/{id}/{name}")
    Call<Folder> updateFolder(@Path("id") int id, @Path("name") String name);

    @DELETE("/users/accounts/folders/{id}")
    Call<Void> deleteFolder(@Path("id") int id);

}
