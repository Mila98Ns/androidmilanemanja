package android2020.retrofit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import android2020.activities.LoginActivity;
import android2020.R;
import android2020.model.Account;

public class SharedPreferencesService {

    public static void setLogin(Context context, Account acc) {

        SharedPreferences.Editor editor = getPreferences(context).edit()
                .putString("username", acc.getUsername())
                .putString("id", String.valueOf(acc.getId()));
        editor.commit();
    }

    public static void setLogout(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit().putString(context.getString(R.string.current_user_name), null);
        editor.commit();
        context.startActivity(new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
