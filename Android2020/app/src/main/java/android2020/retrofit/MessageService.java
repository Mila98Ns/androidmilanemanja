package android2020.retrofit;

import java.util.ArrayList;

import android2020.model.Message;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MessageService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("users/accounts/{id}/messages")
    Call<ArrayList<Message>> getMessages(@Path("id") int id);

    @FormUrlEncoded
    @POST("users/accounts/messages")
    Call<Void> createMessage(
            @Field("id") int accId,
            @Field("to") String to,
            @Field("cc") String cc,
            @Field("bcc") String bcc,
            @Field("subject") String subject,
            @Field("content") String content
    );

    @DELETE("messages/{id}")
    Call<Void> deleteMessage(@Path("id") int id);
}
