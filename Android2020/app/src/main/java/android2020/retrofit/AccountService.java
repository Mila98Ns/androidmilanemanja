package android2020.retrofit;

import android2020.model.Account;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

public interface AccountService {

    @FormUrlEncoded
    @POST("authenticate")
    Call<Account> login(@Field("userName") String username, @Field("password") String password);
}
