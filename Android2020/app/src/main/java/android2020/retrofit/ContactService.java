package android2020.retrofit;

import java.util.ArrayList;

import android2020.model.Contact;
import android2020.model.Folder;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET("users/{id}/contacts")
    Call<ArrayList<Contact>> getContacts(@Path("id") int id);

    @POST("users/{id}/contacts/{first}/{last}/{display}/{email}")
    Call<Contact> addContact(
            @Path("id") int id,
            @Path("first") String first,
            @Path("last") String last,
            @Path("display") String display,
            @Path("email") String email
    );

    @PUT("users/contacts/{cid}/{first}/{last}/{display}/{email}")
    Call<Contact> updateContact(
            @Path("cid") int cid,
            @Path("first") String ime,
            @Path("last") String last,
            @Path("display") String display,
            @Path("email") String email
    );

    @DELETE("users/contacts/{id}")
    Call<Void> deleteContact(@Path("id") int id);

}
