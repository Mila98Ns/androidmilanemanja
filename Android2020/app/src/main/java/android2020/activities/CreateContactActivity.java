package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import android2020.R;
import android2020.model.Contact;
import android2020.model.Message;
import android2020.retrofit.ContactService;
import android2020.retrofit.MessageService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.createcontacts_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                EditText etFirst = findViewById(R.id.etFirstName);
                EditText etLast = findViewById(R.id.etLastName);
                EditText etDisplay = findViewById(R.id.etDisplay);
                EditText etEmail = findViewById(R.id.etEmail);

                ContactService me = RetrofitLib.getClient().create(ContactService.class);

                String first = etFirst.getText().toString();
                String last = etLast.getText().toString();
                String display = etDisplay.getText().toString();
                String email = etEmail.getText().toString();

                if(first.trim().equals("")){
                    Toast.makeText(this,"Unesite first name!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(last.trim().equals("")){
                    Toast.makeText(this,"Unesite last name!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(display.trim().equals("")){
                    Toast.makeText(this,"Unesite display!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(email.trim().contains("@gmail.com")){
                    Toast.makeText(this,"Unesite ispravnu email adresu!",Toast.LENGTH_LONG).show();
                    return true;
                }

                SharedPreferences share = SharedPreferencesService.getPreferences(getApplicationContext());
                String accId = share.getString("id",null);

                Call<Contact> call = me.addContact(Integer.decode(accId), first, last, display, email);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent inte = new Intent(CreateContactActivity.this,ContactsActivity.class);
                startActivity(inte);
                Toast.makeText(this, R.string.CreateFolderActivity_btnSave,Toast.LENGTH_SHORT).show();
                return true;


            case R.id.action_cancel:
                Intent i = new Intent(CreateContactActivity.this,ContactsActivity.class);
                startActivity(i);
                Toast.makeText(this, R.string.CreateFolderActivity_btnCancel,Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
