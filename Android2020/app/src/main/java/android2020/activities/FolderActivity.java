package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android2020.R;
import android2020.model.Contact;
import android2020.model.Folder;
import android2020.retrofit.FolderService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FolderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Folder");

        // Enable back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setHomeButtonEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        Intent in = getIntent();
        Folder folder = (Folder) in.getExtras().getSerializable("Folder");

        TextView folderName = findViewById(R.id.etFolderName);
        folderName.setText(folder.getName());
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.folder_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent fol = getIntent();
        Folder folder = (Folder) fol.getExtras().getSerializable("Folder");

        switch (item.getItemId()) {
            case R.id.action_save:
                EditText etName = findViewById(R.id.etFolderName);
                String name = etName.getText().toString();
                FolderService me = RetrofitLib.getClient().create(FolderService.class);

                if(name.trim().equals("")) {
                    Toast.makeText(this, "Niste uneli naziv foldera!", Toast.LENGTH_LONG).show();
                    return true;
                }

                Call<Folder> call = me.updateFolder(folder.getId(), name);

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent i = new Intent(FolderActivity.this, FoldersActivity.class);
                startActivity(i);
                Toast.makeText(this, R.string.CreateFolderActivity_btnSave,Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_delete:

                FolderService folderService = RetrofitLib.getClient().create(FolderService.class);
                Call<Void> call2 = folderService.deleteFolder(folder.getId());
                call2.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent i = new Intent(FolderActivity.this, FoldersActivity.class);
                        startActivity(i);
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(FolderActivity.this, R.string.FolderActivity_error, Toast.LENGTH_SHORT).show();
                    }
                });
                Toast.makeText(this, R.string.FolderActivity_delete,Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void checkCondition(View view) {
    }
}
