package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import android2020.R;
import android2020.model.Folder;
import android2020.model.Message;
import android2020.retrofit.FolderService;
import android2020.retrofit.MessageService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFolderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create folder");

        // Enable back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setHomeButtonEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.createfolder_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                EditText etName = findViewById(R.id.etNewFolderName);
                String name = etName.getText().toString();
                FolderService me = RetrofitLib.getClient().create(FolderService.class);

                if(name.trim().equals("")) {
                    Toast.makeText(this, "Niste uneli naziv foldera!", Toast.LENGTH_LONG).show();
                    return true;
                }

                SharedPreferences share = SharedPreferencesService.getPreferences(getApplicationContext());
                String accId = share.getString("id",null);

                Call<Folder> call = me.addFolder(Integer.decode(accId), name);

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent i = new Intent(CreateFolderActivity.this, FoldersActivity.class);
                startActivity(i);
                Toast.makeText(this, R.string.CreateFolderActivity_btnSave,Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_cancel:
                Intent inte = new Intent(CreateFolderActivity.this, FoldersActivity.class);
                startActivity(inte);
                Toast.makeText(this, R.string.CreateFolderActivity_btnCancel,Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
