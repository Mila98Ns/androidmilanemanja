package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android2020.R;
import android2020.model.Contact;
import android2020.model.Message;
import android2020.retrofit.ContactService;
import android2020.retrofit.MessageService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEmailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_email);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create email");

        // Enable back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setHomeButtonEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.createemail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:

                EditText etTo = findViewById(R.id.etTO);
                EditText etCc = findViewById(R.id.etCC);
                EditText etBcc = findViewById(R.id.etBCC);
                EditText etSubject = findViewById(R.id.etSubject);
                EditText etContent = findViewById(R.id.etContent);

                String to = etTo.getText().toString();
                String cc = etCc.getText().toString();
                String bcc = etBcc.getText().toString();
                String subject = etSubject.getText().toString();
                String content = etContent.getText().toString();

                if(to.trim().equals("")){
                    Toast.makeText(this,"Unesite primaoca",Toast.LENGTH_LONG).show();
                    return true;
                }

                if(subject.trim().equals("")){
                    Toast.makeText(this,"Unesite subject",Toast.LENGTH_LONG).show();
                    return true;
                }

                SharedPreferences share = SharedPreferencesService.getPreferences(getApplicationContext());
                String accId = share.getString("id",null);

                MessageService me = RetrofitLib.getClient().create(MessageService.class);
                Call<Void> call = me.createMessage(Integer.decode(accId), to, cc, bcc, subject, content);

                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent inte = new Intent(CreateEmailActivity.this,EmailsActivity.class);
                startActivity(inte);
                Toast.makeText(this, R.string.CreateFolderActivity_btnSave,Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_cancel:
                Toast.makeText(this, "Cancel", Toast.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
