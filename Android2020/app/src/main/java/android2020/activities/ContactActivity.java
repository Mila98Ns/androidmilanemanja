package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import android2020.R;
import android2020.model.Contact;
import android2020.model.Message;
import android2020.retrofit.ContactService;
import android2020.retrofit.MessageService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Contact");

        // Enable back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setHomeButtonEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        Intent in = getIntent();
        Contact contact = (Contact) in.getExtras().getSerializable("Contact");

        TextView firstName = findViewById(R.id.etFirstName);
        TextView lastName = findViewById(R.id.etLastName);
        TextView displayName = findViewById(R.id.etDisplay);
        TextView email = findViewById(R.id.etEmail);

        firstName.setText(contact.getFirstName());
        lastName.setText(contact.getLastName());
        displayName.setText(contact.getDisplayName());
        email.setText(contact.getEmail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.contact_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                EditText first = findViewById(R.id.etFirstName);
                EditText last = findViewById(R.id.etLastName);
                EditText display = findViewById(R.id.etDisplay);
                EditText email = findViewById(R.id.etEmail);

                String firstUpdate = first.getText().toString();
                String lastUpdate = last.getText().toString();
                String displayUpdate = display.getText().toString();
                String emailUpdate = email.getText().toString();

                Intent in = getIntent();
                Contact contact = (Contact) in.getExtras().getSerializable("Contact");

                if(firstUpdate.trim().equals("")){
                    Toast.makeText(this,"Unesite first name!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(lastUpdate.trim().equals("")){
                    Toast.makeText(this,"Unesite last name!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(displayUpdate.trim().equals("")){
                    Toast.makeText(this,"Unesite display!",Toast.LENGTH_LONG).show();
                    return true;
                }
                if(emailUpdate.trim().equals("")){
                    Toast.makeText(this,"Unesite email!",Toast.LENGTH_LONG).show();
                    return true;
                }

                ContactService me = RetrofitLib.getClient().create(ContactService.class);
                Call<Contact> call = me.updateContact(contact.getId(),firstUpdate,lastUpdate, displayUpdate, emailUpdate);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                Intent inte = new Intent(ContactActivity.this,ContactsActivity.class);
                startActivity(inte);
                Toast.makeText(this, R.string.contact_updated,Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_cancel:
                Contact c = (Contact) getIntent().getSerializableExtra("Contact");
                ContactService service = RetrofitLib.getClient().create(ContactService.class);
                Call<Void> call2 = service.deleteContact(c.getId());
                call2.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(ContactActivity.this,R.string.contact_deleted,Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ContactActivity.this, ContactsActivity.class);
                        startActivity(i);
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(ContactActivity.this, R.string.FolderActivity_error, Toast.LENGTH_SHORT);
                    }
                });

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
