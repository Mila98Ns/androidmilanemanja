package android2020.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android2020.model.Account;
import android2020.retrofit.AccountService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import android2020.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    AccountService service;
    private ArrayList<Account> accounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnStartEmailsActivity = findViewById(R.id.btnStartEmailsActivity);
        btnStartEmailsActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etUsername = findViewById(R.id.editTextUsername);
                EditText etPassword = findViewById(R.id.editTextPassword);
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                Retrofit retrofit = RetrofitLib.getClient();
                AccountService services = retrofit.create(AccountService.class);
                Call<Account> call = services.login(username,password);
                call.enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        if(response.isSuccessful()){

                            Account acc = response.body();

                            SharedPreferencesService.setLogin(getApplicationContext(), acc);

                            // Go to main Emails activity
                            Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        if(!response.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Pogrešno korisničko ime ili lozinka", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("Greška:",t.getMessage());
                        Log.d("Greška:",t.getStackTrace().toString());
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
