package android2020.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android2020.adapters.ContactAdapter;
import android2020.adapters.DrawerListAdapter;
import android2020.adapters.EmailAdapter;
import android2020.adapters.FolderAdapter;
import android2020.model.Contact;
import android2020.model.Message;
import android2020.model.NavItem;
import android2020.R;
import android2020.retrofit.ContactService;
import android2020.retrofit.MessageService;
import android2020.retrofit.RetrofitLib;
import android2020.retrofit.SharedPreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;

public class ContactsActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    private ArrayList<Contact> allContacts = new ArrayList<>();
    private FloatingActionButton actionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        SharedPreferences share1 = SharedPreferencesService.getPreferences(getApplicationContext());
        String usr = share1.getString("username",null);
        TextView username = findViewById(R.id.userName);
        username.setText(usr);

        actionButton = findViewById(R.id.contactsFAB);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactsActivity.this , CreateContactActivity.class);
                startActivity(i);
                return;
            }
        });

        // TOOLBAR

        prepareMenu(mNavItems);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mDrawerList = findViewById(R.id.navList);

        mDrawerPane = findViewById(R.id.drawerPane);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);

        // postavljamo senku koja preklama glavni sadrzaj
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        // drawer-u postavljamo unapred definisan adapter
        mDrawerList.setAdapter(adapter);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setIcon(R.drawable.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        actionBar.setTitle("Contacts");
        actionBar.setDisplayHomeAsUpEnabled(true);


        /*
         * drawer-u specificiramo za koju referencu toolbar-a da se veze
         * Specificiramo sta ce da pise unutar Toolbar-a kada se drawer otvori/zatvori
         * i specificiramo sta ce da se desava kada se drawer otvori/zatvori.
         * */
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("SupportActionBarTitle");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Izborom na neki element iz liste, pokrecemo akciju
        if (savedInstanceState == null) {
//            selectItemFromDrawer(0);
        }

        // Set profileBox onClick listener
        RelativeLayout profileBox = findViewById(R.id.profileBox);
        profileBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactsActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        SharedPreferences share = SharedPreferencesService.getPreferences(getApplicationContext());
        String accId = share.getString("id",null);

        ContactService ms = RetrofitLib.getClient().create(ContactService.class);
        Call<ArrayList<Contact>> call = ms.getContacts(Integer.decode(accId));

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Call<ArrayList<Contact>> call, Response<ArrayList<Contact>> response) {
                allContacts = response.body();

                ListView mList = findViewById(R.id.listView_contacts);
                ContactAdapter ca = new ContactAdapter(getApplicationContext(),allContacts.size(),allContacts);
                mList.setAdapter(ca);

                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                    {
                        Intent contact = new Intent(ContactsActivity.this, ContactActivity.class);
                        contact.putExtra("Contact", allContacts.get(position));
                        startActivity(contact);
                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<Contact>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {

        mDrawerList.setItemChecked(position, true);
        setTitle(mNavItems.get(position).getmTitle());
        mDrawerLayout.closeDrawer(mDrawerPane);

        if(position == 0){
            // emails
            startActivity(new Intent(this, EmailsActivity.class));
        }
        else if(position == 1){
            // contacts
            startActivity(new Intent(this, ContactsActivity.class));
        }
        else if(position == 2){
            // contacts
            startActivity(new Intent(this, FoldersActivity.class));
        }
        else if(position == 3){
            // settings
            startActivity(new Intent(this, SettingsActivity.class));
        }

        else if(position == 4){
            // logout
            SharedPreferencesService.setLogout(this);
        }
        else{
            Log.e("DRAWER", "Van opsega!");
        }
    }

    private void prepareMenu(ArrayList<NavItem> mNavItems ) {
        mNavItems.add(new NavItem(getString(R.string.nav_emails)));
        mNavItems.add(new NavItem(getString(R.string.nav_contacts)));
        mNavItems.add(new NavItem(getString(R.string.nav_folders)));
        mNavItems.add(new NavItem(getString(R.string.nav_settings)));
        mNavItems.add(new NavItem(getString(R.string.nav_logout)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.contacts_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                Intent i = new Intent(ContactsActivity.this , CreateContactActivity.class);
                startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
