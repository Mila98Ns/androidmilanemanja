package android2020.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import android2020.R;
import android2020.model.Folder;
import android2020.model.Rule;

public class FolderAdapter extends ArrayAdapter<Folder> {

    private Context context;
    int resource;
    private ArrayList<Folder> folders;

    public FolderAdapter(Context context,int resourcel, ArrayList<Folder> folders) {
        super(context, R.layout.adapter_folders,folders);
        this.context = context;
        this.resource = resourcel;
        this.folders = folders;
    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        Folder f = folders.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_folders, parent, false);
        }

        TextView txtName = convertView.findViewById(R.id.name);
        TextView txtbrojPoruka = convertView.findViewById(R.id.brojPoruka);

        String name = getItem(position).getName();

        txtName.setText(name);
        //txtbrojPoruka.setText();

        return convertView;
    }
}