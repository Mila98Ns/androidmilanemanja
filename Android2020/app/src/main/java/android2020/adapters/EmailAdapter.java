package android2020.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import android2020.R;
import android2020.model.Message;

public class EmailAdapter extends ArrayAdapter<Message> {

    private Context context;
    private ArrayList<Message> emails;

    public EmailAdapter(Context context, int resource, ArrayList<Message> emails) {
        super(context, resource, emails);
        this.context = context;
        this.emails = emails;
    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        Message m = emails.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_emails, parent, false);
        }

        TextView txtNazivPosiljaoca = convertView.findViewById(R.id.nazivPosiljaoca);
        TextView txtPredmetPoruke = convertView.findViewById(R.id.predmetPoruke);
        TextView txtKratakSadrzaj = convertView.findViewById(R.id.kratakSadrzaj);
        TextView txtDatum = convertView.findViewById(R.id.datum);

        txtNazivPosiljaoca.setText(m.getFrom());

        String predmetPoruke = m.getSubject();
        if(predmetPoruke.length() > 20 ){
            predmetPoruke = predmetPoruke.substring(0,16) + "...";
        }
        txtPredmetPoruke.setText(predmetPoruke);

        String kratakSadrzaj= m.getContent();
        if(kratakSadrzaj.length() > 35 ){
            kratakSadrzaj = kratakSadrzaj.substring(0,31) + "...";
        }
        txtKratakSadrzaj.setText(kratakSadrzaj);

        Date datum  = m.getDateTime();
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        df.setTimeZone(tz);

        String novoVreme = df.format(datum);
        txtDatum.setText(novoVreme);

        return convertView;
    }
}