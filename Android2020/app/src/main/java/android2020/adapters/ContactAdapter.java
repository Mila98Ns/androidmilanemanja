package android2020.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import android2020.R;
import android2020.model.Contact;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private Context context;
    private ArrayList<Contact> contacts;

    public ContactAdapter(Context context, int resource, ArrayList<Contact> contacts) {
        super(context, resource, contacts);
        this.context = context;
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public View getView(final int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        Contact c = contacts.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_contacts, parent, false);
        }

        ImageView img = convertView.findViewById(R.id.image);
        TextView txtFirstName = convertView.findViewById(R.id.firstName);
        TextView txtLastName = convertView.findViewById(R.id.lastName);

        txtFirstName.setText(c.getFirstName());
        txtLastName.setText(c.getLastName());

        return convertView;
    }
}