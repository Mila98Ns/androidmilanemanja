package android2020.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Account implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("smtp")
    private String smtp;
    @SerializedName("pop3Imap")
    private String pop3Imap;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;

    public Account() {
    }

    public Account(String smtp, String pop3Imap, String username, String password) {
        this.smtp = smtp;
        this.pop3Imap = pop3Imap;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPop3Imap() {
        return pop3Imap;
    }

    public void setPop3Imap(String pop3Imap) {
        this.pop3Imap = pop3Imap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
