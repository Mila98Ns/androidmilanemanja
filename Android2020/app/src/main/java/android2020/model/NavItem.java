package android2020.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NavItem implements Parcelable {

    private String mTitle;

    public NavItem(String title) {
        mTitle = title;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    @Override
    public String toString() {
        return mTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mTitle);
    }
}