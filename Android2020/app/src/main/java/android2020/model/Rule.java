package android2020.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rule implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("condition")
    private String condition;
    @SerializedName("operation")
    private String operation;

    public Rule(String condition, String operation) {
        this.condition = condition;
        this.operation = operation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
